------------------------
ReciteMe
------------------------

It's a javaScript based utility to enable the Recite Me accessibility widget for the Drupal website. It is a useful tool to make site Accessibile for everyone.

Install & set up the ReciteMe Connector Drupal module, as described below.

Installation
------------

1) Download the module and place it with other contributed modules
(e.g. modules/contrib).

2) Enable the ReciteMe Connector module on the Modules list page.

3) Go to ReciteMe module configuration to add the details about ReciteMe API and keys.

4) Go to block layout configuration and place the ReciteMe block in the desired region of the drupal region.

Set up ReciteMe
------------

Please visit the ReciteMe official site https://reciteme.com for details.

Here we just need the following details about Recite Me API.

- Sercice URL
- Service Key

Extend
-------
- Hook_theme_hook_alter for extending default template of ReciteMe.
- Override default CSS libraries to change default UI/UX.

The module comes with a simple twig template called "recite-me-block.html.twig"
you can copy this template in your theme and customize it.
The more important thing to take into account is the way in which the HTML Tag is rendered into this template

{{ html_tag | raw }}

Don't forget the use of the "raw" filter to render properly the tag, otherwise, the HTML Tags as plain text will be output.
<?php

namespace Drupal\reciteme_connector\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ReciteMeBlock' block.
 *
 * @Block(
 *  id = "recite_me_block",
 *  admin_label = @Translation("ReciteMe block"),
 * )
 */
class ReciteMeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'recite_me_block';
    $config = \Drupal::config('reciteme_connector.recitemeconfig');
    if($config->get('is_enable_reciteme') == 1){
      $build['#attached']['library'][] = 'reciteme_connector/reciteme';
      $build['#attached']['drupalSettings']['reciteme']['serveiceURL'] = $config->get('sercice_url');
      $build['#attached']['drupalSettings']['reciteme']['serveiceKey'] = $config->get('service_key');
      $build['#attached']['drupalSettings']['reciteme']['is_autoload'] = $config->get('is_autoload');
      $build['#attached']['drupalSettings']['reciteme']['enableFregment'] = $config->get('enable_fregment');
      $build['#enable_fregment'] = $config->get('enable_fregment');
      $build['#widget_bg_image'] = $config->get('widget_bg_img_url');
      $build['#widget_bg_image_hover'] = $config->get('widget_hover_img_url');
      $build['#widget_txt'] = $config->get('widget_txt');
    }

    return $build;
  }

}

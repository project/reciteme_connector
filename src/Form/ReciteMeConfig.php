<?php

namespace Drupal\reciteme_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Class ReciteMeConfig.
 */
class ReciteMeConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'reciteme_connector.recitemeconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reciteme_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('reciteme_connector.recitemeconfig');
    $form['sercice_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sercice URL'),
      '#description' => $this->t('Service URL'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('sercice_url'),
      '#required' => TRUE,
    ];
    $form['service_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Key'),
      '#description' => $this->t('Service Key.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('service_key'),
      '#required' => TRUE,
    ];
    $form['enable_fregment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enable Fregment'),
      '#description' => $this->t('Enable fregment. Ideally it will be CSS selector like - Id, Class etc.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('enable_fregment'),
      '#required' => TRUE,
    ];
    $form['is_autoload'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autoload'),
      '#description' => $this->t('Is autoload?'),
      '#default_value' => $config->get('is_autoload'),
    ];
    $form['is_enable_reciteme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Recite me through out the site'),
      '#description' => $this->t('Is Enable Recite me through out the site?'),
      '#default_value' => $config->get('is_enable_reciteme'),
    ];
    $form['widget_txt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget Text'),
      '#description' => $this->t('Widget link text. It will be used if widget <b>Background Image and hover image</b> are not uploaded.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('widget_txt'),
      '#required' => TRUE,
    ];
    $form['widget_bg_img'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Widget Background Image'),
      '#description' => $this->t('Select an image for recite me widget background image'),
      '#default_value' => $config->get('widget_bg_img'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('png jpg jpeg'),
        ),
      '#upload_location' => 'public://recite_me/',
    ];
    $form['widget_hover_img'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Widget Hover Image'),
      '#description' => $this->t('Select an image for recite me widget hover image'),
      '#default_value' => $config->get('widget_hover_img'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('png jpg jpeg'),
        ),
      '#upload_location' => 'public://recite_me/',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $img_url = '';
    $hover_img_url = '';
    if(!empty($form_state->getValue('widget_bg_img'))){
      $fid = current($form_state->getValue('widget_bg_img'));
      $file = File::load($fid);
      $img_url = $file->createFileUrl();
    }
    if(!empty($form_state->getValue('widget_hover_img'))){
      $fid = current($form_state->getValue('widget_hover_img'));
      $file = File::load($fid);
      $hover_img_url = $file->createFileUrl();
    }
    $this->config('reciteme_connector.recitemeconfig')
      ->set('sercice_url', $form_state->getValue('sercice_url'))
      ->set('service_key', $form_state->getValue('service_key'))
      ->set('is_autoload', $form_state->getValue('is_autoload'))
      ->set('enable_fregment', $form_state->getValue('enable_fregment'))
      ->set('is_enable_reciteme', $form_state->getValue('is_enable_reciteme'))
      ->set('widget_bg_img', $form_state->getValue('widget_bg_img'))
      ->set('widget_bg_img_url', $img_url)
      ->set('widget_hover_img', $form_state->getValue('widget_hover_img'))
      ->set('widget_hover_img_url', $hover_img_url)
      ->set('widget_txt', $form_state->getValue('widget_txt'))
      ->save();
  }
}
